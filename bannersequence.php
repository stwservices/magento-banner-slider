<div id="sequence-theme">
    <div id="sequence" style="width:80%; margin-top: 50px;">
       
        <ul class="controls">
            <li><a class="sequence-prev">Prev</a></li>
            <li><a class="sequence-next">Next</a></li>
        </ul> 
      
		<ul class="sequence-canvas">
			<li class="animate-in">
                <h2 class="title" style="">Home Promo</h2>
			    <h3 class="subtitle1" style="">50% Sale</h3>
					<div class="intro subtitle" style="">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
					</div>
					<img alt="Image" src="images/men.png" class="slider-bg" style="">
			</li>
			<li class="animate-out">
				<h2 class="title" style="">Free Product </h2>
			    <h3 class="subtitle1" style="">on every 2 product</h3>
					<div class="intro subtitle" style="">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
					</div>
					<img alt="Image" src="images/party_dresses_banner4.jpg" class="slider-bg" style="">
			</li>
        </ul>        
    </div>
</div>
<link rel="stylesheet" type="text/css" href="css/bannersequence.css">			
<script src="js/bannerslider/ma.jq.slide.js"></script>
<script src="js/bannerslider/jquery.sequence-min.js"></script>
<script type ="text/javascript">	
    //<![CDATA[
    $jq(document).ready(function(){       
        var options = {
            autoPlay: true,
            autoPlayDelay: 2000,
            pauseOnHover: true, 
            hidePreloaderDelay: 500,
            nextButton: true,
            prevButton: true,
            pauseButton: true,
            preloader: true,
            pagination:true,
            hidePreloaderUsingCSS: false,                   
            animateStartingFrameIn: true,    
            navigationSkipThreshold: 750,
            preventDelayWhenReversingAnimations: true,
            customKeyEvents: {
                80: "pause"
            }
        };

        var sequence = $jq("#sequence").sequence(options).data("sequence");
           
    });
    //]]>

</script>